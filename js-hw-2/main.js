"use strict"

// 1) Які існують типи даних у Javascript?
// string, number, boolean, null, undefined, symbol, object, bigInt

// 2) У чому різниця між == і ===?
//  == не враховує тип даних; === враховує тип даних.

// 3) Що таке оператор?
//  Дія, яку потрібно виконати ( +, -, = і т.д.)


let userName = prompt("What is your name?");

while (userName === null || userName === "" || userName === " " ) {
  userName = prompt("What is your name?", userName);
}

let userAge = prompt("How old are you?");

while (userAge === null || userAge === "" || isNaN(userAge) || typeof +userAge !== "number")  {
 userAge = prompt("How old are you?", userAge)
}

 if (userAge < 18) {
    alert("You are not allowed to visit this website");
} 

else if (userAge <= 22) {
 let userDesigion = confirm("Are you sure you want to continue?");

  if ( userDesigion === true) {
        alert("Welcome " + userName);
  } else {
    alert("You are not allowed to visit this website");
  }
}

else {
    alert("Welcome " + userName);
}