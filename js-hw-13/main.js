"use strict"

// 1) Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// 2) Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// 3) Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

// 1) setTimeout викликає функцію один раз через заданий інтервал часу, а setInterval викликає її постійно повторюючи виклик через заданий промжок часу.

// 2)  тоді функція буде викликана без затримки, але після завершення поточного коду.

// 3)  Оскільки setInterval() виконує функцію періодично без зупинки він буде впливати на наступний код. Тому його відключають за допомогою clearInterval().

// При запуску програми на екрані має відображатись перша картинка.
// Через 3 секунди замість неї має бути показано друга картинка.
// Ще через 3 секунди – третя.
// Ще через 3 секунди – четверта.
// Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.

const images = document.querySelector(".images-wrapper");
const imgArr = Array.from(images.children);

imgArr.forEach(element => {
    element.style.display = "none";
});
const mainImg = document.createElement("div");
document.body.append(mainImg);

const imgSrc = document.createElement("img");
imgSrc.style.cssText = "width: 400px; height: 400px";
imgSrc.src = imgArr[0].src;
mainImg.append(imgSrc);

let count = 0;

function tick() {
    count = count +1;
    imgSrc.src = imgArr[count].src;
    timerId = setTimeout(tick, 3000);
    setInterval(() => {
        if (count === imgArr.length -1) {
            count = -1
        }
       }, 0);
 } 
let timerId = setTimeout(tick , 3000);

const btnPause = document.createElement("button");
btnPause.textContent = "Припинити";

const btnStart = document.createElement("button");
btnStart.textContent = "Відновити показ";

mainImg.after(btnPause);
btnPause.after(btnStart);

btnPause.addEventListener("click", (() =>{
    clearTimeout(timerId);
}))
btnStart.addEventListener("click", (() =>{
    clearTimeout(timerId);
    timerId = setTimeout(tick , 3000);
}))
