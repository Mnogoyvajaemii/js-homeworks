"use strict"

const form = document.querySelector(".password-form");

function showPassword (event){
    const target = event.target;
    if (target.tagName != "I") {
    return;
}
    changeIcon(target);
}

function changeIcon(icon) {
    const label = icon.closest('label');
    const slashIcon =label.querySelector(".fa-eye-slash ");
    const input = label.querySelector(".input-field");

    slashIcon.style.display = "none";
    input.setAttribute("type", "password");

    if (icon.classList.contains("fa-eye")) {
        slashIcon.style.display = "inline-block";
        input.setAttribute("type", "text");
    };
    
}

const errorText = document.createElement('p');

function checkPasswords () {
    const upperInput = document.querySelector("#upperInput");
    const lowerInput = document.querySelector("#lowerInput");
   
    if ( upperInput.value === lowerInput.value && upperInput.value !== "" ) {
        alert("You are welcome")
        errorText.textContent = " ";
        upperInput.value = " ";
        lowerInput.value = " ";

    } else if (upperInput.value !== lowerInput.value){
        errorText.style.color = "red";
        errorText.textContent = "Потрібно ввести однакові значення";
        lowerInput.after(errorText);
    } 
}

form.addEventListener("click", showPassword);
form.addEventListener("submit", checkPasswords);



