"use strict"

const changeThemeBtn = document.querySelector(".change-theme-btn");

const secondTheme = document.createElement("link");
secondTheme.href = "css/styles-second-theme.css";
secondTheme.rel = "stylesheet";
secondTheme.id = "secondTheme";

function changeColor (){
    if (document.querySelector("#secondTheme") !== null) {
        secondTheme.remove();
        localStorage.removeItem("theme")
    } else {
        document.head.append(secondTheme);
        localStorage.setItem("theme", "blue theme");
    }
}

function ready() {
    if (localStorage.theme === "blue theme") {
        document.head.append(secondTheme);
    }
}
changeThemeBtn.addEventListener("click",changeColor);
document.addEventListener("DOMContentLoaded", ready);
