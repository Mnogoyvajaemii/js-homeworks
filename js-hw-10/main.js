"use strict"

const tabs = document.querySelector(".tabs");
const tabsTiteles = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tabs-content-text");

function onTabsClick (event){
  const target = event.target;
  highlight(target);
  showTabsText()
}

function highlight (selectedLi) {
  tabsTiteles.forEach((element) => {
   element.classList.remove('active');
  })
  selectedLi.classList.add('active');
} 

tabs.addEventListener("click", onTabsClick);

const tabsTitelesArr = Array.from(tabsTiteles);
const tabsContentArr = Array.from(tabsContent);

function showTabsText () {  
  let number = 0
 tabsTitelesArr.findIndex((item, index )=>{
    if (item.classList.contains('active')) {
     number = index ; }
    })

    tabsContentArr.forEach((element) =>{
      element.style.display = "none";
      tabsContentArr[number].style.display = ''
    })

  }
 showTabsText () 

