"use strict"

// Чому для роботи з input не рекомендується використовувати клавіатуру?

//  Самих подій клавіатури буде недостатньо, бо в поле input дані можна ще й скопіювати/вставити за допомогою миші, скористатись голосовим розпізнаванням... Тому краще використовувати події: change, input, cut, copy, paste.

const buttons = document.querySelectorAll(".btn")

function changeColor (event) {
buttons.forEach((element) => {
    element.style.backgroundColor = "black";
})
switch (event.code) {
    case "Enter":
        buttons[0].style.backgroundColor = "blue";
        break
    case "KeyS":
        buttons[1].style.backgroundColor = "blue";
        break;
    case "KeyE":
         buttons[2].style.backgroundColor = "blue";
         break;
     case "KeyO":
        buttons[3].style.backgroundColor = "blue";
        break;
    case "KeyN":
        buttons[4].style.backgroundColor = "blue";
        break;
    case "KeyL":
         buttons[5].style.backgroundColor = "blue";
         break;
    case "KeyZ":
         buttons[6].style.backgroundColor = "blue";
        break;

}        

}

window.addEventListener("keydown", changeColor)