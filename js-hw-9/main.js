"use strict"

//  1)Опишіть, як можна створити новий HTML тег на сторінці.
//  2)Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//  3)Як можна видалити елемент зі сторінки?

//  1) Методом createElement()

//  2) Перший параметр означає місце вставки елементу. Можна вставити необхідний елемент перед перед іншим елементом (beforebegin), в іншому елементі спочатку (afterbegin), в іншому елементі  в кінці (beforeend), та після іншого елементу (afterend).

//  3) Методом remove()

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:

const testArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const testArr2 = ["1", "2", "3", "sea", "user", 23];

function createList(arr, parent =document.body) {
    const fragment = document.createDocumentFragment();
    const list = document.createElement("ol")

    for (let i = 0; i < arr.length; i++) {
     const innerLi = document.createElement("li")
        innerLi.textContent = arr[i]
     fragment.append(innerLi)
}
    list.append(fragment)
    parent.append(list)
}

createList(testArr2, )
