"use strict"


// 1) Опишіть своїми словами що таке Document Object Model (DOM)
//  2) Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//  3) Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// 1) Структура документу у вигляді вузлів та об'єктів, яка дає можливість звертатить і змінювати об'єкти, наповнення в документі.

//  2) innerText показує лише текст всередині тегу, а innerHTML  показує також HTML структуру елементу.

//  3)  getElementById, getElementsByClassName, getElementsByTagName, querySelector та querySelectorAll. Напевно останні два найкращі.

// Завдання

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let paragraphs = document.querySelectorAll("p")
for (const elem of paragraphs) {
    elem.style.backgroundColor = "#ff0000"
}

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

console.log(document.getElementById("optionsList"))

console.log(document.getElementById("optionsList").parentElement)

let list = document.getElementById("optionsList")
if (list.hasChildNodes()) {
    list = list.childNodes;
    for (const nodes of list) {
        console.log(` Назва: ${nodes.nodeName}; тип: ${nodes.nodeType}; `);
    }
}


// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
// document.getElementById("testParagraph").innerText = "This is a paragraph"

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
let header = document.querySelector(".main-header").children
for (const i of header) {
    i.classList.add('nav-item')
    console.log(i);
}


// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let section = document.querySelectorAll(".section-title")
for (const i of section) {
    i.classList.remove("section-title")
    console.log(i);
}




